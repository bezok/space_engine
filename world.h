#ifndef WORLD_H
#define WORLD_H

#include <QObject>
#include <QTimer>
#include <memory>
#include <vector>
#include <Box2D/Box2D.h>
#include "entity.h"
#include "bullet.h"
#include "creature.h"
#include "misc.h"

class World : public QObject, b2World
{
    Q_OBJECT
public:
    World();

Q_SIGNALS:

public Q_SLOTS:
    void add_entity(std::unique_ptr<Entity> &entity);
    void delete_entity(unsigned id);
    void update();

private:
    void create_entity(std::string script);
    void create_body(PyObject *module, Entity *entity);

    QTimer update_timer;
    unsigned entity_id;

    std::vector<std::unique_ptr<Entity>> entities;
};



class ContactListener : public b2ContactListener
{
public:
    void BeginContact(b2Contact *contact)
    {
        Entity *a = static_cast<Entity *>(contact->GetFixtureA()->GetBody()->GetUserData());
        Entity *b = static_cast<Entity *>(contact->GetFixtureA()->GetBody()->GetUserData());
        QTimer::singleShot(0, a, [a, b](){a->collide(b);});
        QTimer::singleShot(0, b, [a, b](){b->collide(a);});
    }
};


#endif // WORLD_H
