#ifndef BULLET_H
#define BULLET_H

#include "entity.h"
#include "creature.h"

class Bullet : public Entity
{
public:
    Bullet(PyObject *module);

    float get_damage();

Q_SIGNALS:


public Q_SLOTS:
    void collide(Entity *collide_entity);

private:
    float damage;
};

#endif // BULLET_H
