#ifndef ENTITY_H
#define ENTITY_H

#include <Python.h>
#include <QObject>
#include <QTimer>
#include <Box2D/Box2D.h>
#include "misc.h"

class Entity : public QObject
{
    Q_OBJECT
public:
    explicit Entity(QObject *parent = 0);

    void set_body(b2Body *body);
    std::string & get_type();
    unsigned get_id();

Q_SIGNALS:

public Q_SLOTS:
    void collide(Entity *collide_body);
    void update();

protected:
    b2Body *body;
    std::string type;

private:
    unsigned id;
    QTimer update_timer;
};

#endif // ENTITY_H
