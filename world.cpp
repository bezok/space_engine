#include "world.h"

World::World()
    : b2World(b2Vec2(0.0f, 0.0f))
{
    connect(&this->update_timer, SIGNAL(timeout()), this, SLOT(update()));
    this->update_timer.start(16);
}



void World::add_entity(std::unique_ptr<Entity> &entity)
{
    this->entities.push_back(std::move(entity));
}



void World::delete_entity(unsigned id)
{
    auto it = find_if(this->entities.begin(), this->entities.end(), [&id](const std::unique_ptr<Entity> &entity){return entity->get_id() == id;});
    auto index = std::distance(this->entities.begin(), it);

    this->entities.erase(this->entities.begin() + index);
}



void World::update()
{

}



void World::create_entity(std::string script)
{
    PyObject *module = PyImport_ImportModule(script.c_str());
    std::unique_ptr<Entity> entity;

    auto type = get_string_attribute(module, "type");
    if (type == "creature")
    {
        entity.reset(new Creature(module));
    }
    else if (type == "bullet")
    {
        entity.reset(new Bullet(module));
    }

    this->create_body(module, entity.get());
    this->add_entity(entity);
}



void World::create_body(PyObject *module, Entity *entity)
{
    b2BodyDef body_def;
    b2FixtureDef fixture_def;
    b2CircleShape shape;

    body_def.angularDamping = get_float_attribute(module, "a_damping");
    body_def.linearDamping = get_float_attribute(module, "l_damping");
    body_def.bullet = get_float_attribute(module, "bullet");
    body_def.type = b2_dynamicBody;
    body_def.userData = entity;
    auto body = this->CreateBody(&body_def);
    entity->set_body(body);

    fixture_def.density = get_float_attribute(module, "density");
    fixture_def.friction = get_float_attribute(module, "friction");
    fixture_def.restitution = get_float_attribute(module, "restitution");
    shape.m_radius = get_float_attribute(module, "size");
    fixture_def.shape = &shape;
    body->CreateFixture(&fixture_def);
}
