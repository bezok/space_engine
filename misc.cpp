#include "misc.h"


float get_float_attribute(PyObject *module, std::string attribute)
{
    auto attr = PyObject_GetAttrString(module, attribute.c_str());
    if (attr)
    {
        return PyFloat_AsDouble(attr);
    }
    else
    {
        PyErr_Print();
        return 0;
    }
}



char * get_string_attribute(PyObject *module, std::string attribute)
{
    auto attr = PyObject_GetAttrString(module, attribute.c_str());
    if (attr)
    {
        return PyUnicode_AsUTF8(attr);
    }
    else
    {
        PyErr_Print();
        return 0;
    }
}
