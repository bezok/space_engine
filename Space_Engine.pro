#-------------------------------------------------
#
# Project created by QtCreator 2017-08-05T14:18:30
#
#-------------------------------------------------

CONFIG += no_keywords
QT       += network

QT       -= gui

TARGET = Space_Engine
TEMPLATE = lib

DEFINES += SPACE_ENGINE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    world.cpp \
    entity.cpp \
    creature.cpp \
    misc.cpp \
    bullet.cpp

HEADERS += \
    world.h \
    entity.h \
    creature.h \
    misc.h \
    bullet.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/release/ -lpython3.5m
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/debug/ -lpython3.5m
else:unix: LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/ -lpython3.5m

INCLUDEPATH += $$PWD/../../../../usr/include/python3.5m
DEPENDPATH += $$PWD/../../../../usr/include/python3.5m
