#include "entity.h"

Entity::Entity(QObject *parent) : QObject(parent)
{


    connect(&this->update_timer, SIGNAL(timeout()), this, SLOT(update()));
    this->update_timer.start(33);
}



void Entity::set_body(b2Body *body)
{
    this->body = body;
}



void Entity::collide(Entity *collide_entity)
{

}



void Entity::update()
{

}


std::string & Entity::get_type()
{
    return this->type;
}



unsigned Entity::get_id()
{
    return this->id;
}



