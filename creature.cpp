#include "creature.h"

Creature::Creature(PyObject *module)
{
    this->type = "creature";

    this->agility = get_float_attribute(module, "agility");
    this->vitality = get_float_attribute(module, "vitality");
    this->strength = get_float_attribute(module, "strength");
    this->hp = this->vitality;

    connect(&this->attack_timer, SIGNAL(timeout()), this, SLOT(attack()));
    connect(&this->health_update_timer, SIGNAL(timeout()), this, SLOT(health_update()));
    connect(&this->move_timer, SIGNAL(timeout()), this, SLOT(move()));

    this->health_update_timer.start(1000);
}



void Creature::collide(Entity *collide_entity)
{

}



void Creature::set_attack()
{

}



void Creature::set_move(float x, float y)
{
    this->x = x;
    this->y = y;
    this->move_timer.start(16);
}



void Creature::increase_exp(float exp)
{
    this->exp += exp;
    if (this->exp > lvl * 1000)
    {
        lvl++;
        this->exp -= lvl * 1000;
    }
}



void Creature::health_update()
{
    if (this->hp < this->vitality)
    {
        this->hp++;
        if (this->hp > this->vitality)
        {
            this->hp = this->vitality;
        }
    }
}



void Creature::hit(float damage)
{
    this->hp -= damage;
}



void Creature::move()
{
    b2Vec2 position = this->body->GetPosition();
    position.x -= this->x;
    position.y -= this->y;

    if (abs(position.x) > 0.1 and abs(position.y) > 0.1)
    {
        b2Vec2 force;
        b2Vec2 velocity = this->body->GetLinearVelocity();
        float mass = this->body->GetMass();
        force.x = mass * abs(velocity.x * position.x);
        force.y = mass * abs(velocity.y * position.y);

        if (force.x > this->agility)
        {
            force.x = this->agility;
            if (position.x < 0)
            {
                force.x *= -1;
            }
        }

        if (force.y > this->agility)
        {
            force.y = this->agility;
            if (position.y < 0)
            {
                force.y *= -1;
            }
        }

        this->body->ApplyForceToCenter(force, true);
    }
    else
    {
        this->move_timer.stop();
    }
}
