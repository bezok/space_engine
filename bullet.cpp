#include "bullet.h"

Bullet::Bullet(PyObject *module)
{
    this->type = "bullet";
}



void Bullet::collide(Entity *collide_entity)
{
    if (collide_entity->get_type() == "creature")
    {
        Creature *creature = static_cast<Creature *>(collide_entity);
        QTimer::singleShot(0, creature, [this, creature](){creature->hit(this->damage);});
    }
}



float Bullet::get_damage()
{
    return this->damage;
}
