#ifndef MISC_H
#define MISC_H


#include <Python.h>
#include <string>


float get_float_attribute(PyObject *module, std::string attribute);
char * get_string_attribute(PyObject *module, std::string attribute);


#endif // MISC_H
