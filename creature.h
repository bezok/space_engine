#ifndef CREATURE_H
#define CREATURE_H

#include "bullet.h"
#include "entity.h"

class Creature : public Entity
{
public:
    Creature(PyObject *module);

Q_SIGNALS:
    void deal_damage();

public Q_SLOTS:
    void hit(float damage);
    void set_attack();
    void set_move(float x, float y);

private Q_SLOTS:
    void collide(Entity *collide_entity);
    void increase_exp(float exp);
    void health_update();
    void move();

private:
    QTimer attack_timer;
    QTimer health_update_timer;
    QTimer move_timer;

    float agility;
    float strength;
    float vitality;
    float hp;

    float x;
    float y;

    float exp;
    unsigned int lvl;
};

#endif // CREATURE_H
